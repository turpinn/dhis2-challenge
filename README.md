# Deployed version
https://dhis2-challenge.vercel.app/

# Install dependencies
cd dhis2-challenge

npm install

# Run local server
npm run dev

- The app will run on http://localhost:5173/ by default.

# Run tests
npm test

# About
This is a response to the DHIS2 technical assesment, Option 1.
Submitted by Nadia Rincon Turpin - nrturpin@gmail.com on November 08 2023

# Assumptions and design choices
- I display chess pieces as ascii chars (just naming it in case some other font does not display it correctly)
- I have assumed that empty spaces cannot be selected, unless you want to move something there (previously selected)
- Selected squares have yellow background
- I am not taking care of alternate turns between players, as it was not specified.
- FEN notation: from the Wikipedia spec, I only handle the first field, describing the chessboard. I do not handle, parse or display the other fields (example: w KQkq - 0 1) 
- The events for the reducer seem to trigger more than once. Apparently the cause is that I am using StrictMode (see main.jsx)
- For simplicity, FEN validation only happens after clicking on the "validate" button. An error message displays by the button if the FEN string is invalid.
- Persistence: I have sneaked the local storage code in the existing reducer, to keep it very simple. I have seen examples where they create a wrapper reducer that persists and uses the chess logic reducer.
- Avoid regexp: I could have used regular expressions, at least partially, to validate FEN strings. I decided to avoid it in the validation code for better readability - I wrote code searching for invalid characters instead.
- About tests: since the spec only said "some tests" I just wrote some very simple tests focusing mostly on the validation of FEN strings. In a real project I would have tested the reducer, but I left it out for simplicity.

# More info: React + Vite
This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh
