import { useState, useReducer } from 'react';
import { ChessBoard } from './Components.jsx';
import { chessReducer, getInitialFenStatus } from './Reducer.js';
import { isValidFenString } from './Utils.js';

const initialFenValue = getInitialFenStatus();

const initialGameStatus = {
  fenStatus: initialFenValue,
  inputFieldValue: initialFenValue,
  selectedRow: NaN,
  selectedColumn: NaN,
};

export default function App() {

  const [gameStatus, dispatch] = useReducer(
    chessReducer, initialGameStatus
  );

  const validateButtonDisabled = gameStatus.fenStatus === gameStatus.inputFieldValue;
  var [errorMessage, setErrorMessage] = useState('');

  function onPlay(rowNr, columnNr) {
    if (isNaN(gameStatus.selectedRow)) {
      handleSetSelected(rowNr, columnNr);
    } else if (rowNr === gameStatus.selectedRow && columnNr === gameStatus.selectedColumn) {
      handleUndoSelection();
    } else {
      handleMoveToOrToggle(rowNr, columnNr);
    }
  }

  function handleMoveToOrToggle(rowNr, columnNr) {
    dispatch({
      type: 'moveToOrToggle',
      row: rowNr,
      column: columnNr,
    });
  }

  function handleSetSelected(rowNr, columnNr) {
    dispatch({
      type: 'setSelected',
      row: rowNr,
      column: columnNr,
    });
  }

  function handleUndoSelection() {
    dispatch({
      type: 'undoSelection'
    });
  }

  function handleSetNewFenStatus(newFenStatus) {
    setErrorMessage('')
    dispatch({
      type: 'setFenStatus',
      newFenStatus: newFenStatus
    });
  }

  function handleFenStringChange(e) {
    const enteredValue = e.target.value;
    dispatch({
      type: 'setInputFieldValue',
      newValue: enteredValue
    });
    setErrorMessage("")
  }

  function validateFenString() {
    if (isValidFenString(gameStatus.inputFieldValue)) {
      handleSetNewFenStatus(gameStatus.inputFieldValue);
    } else {
      setErrorMessage("The FEN string is not valid");
    }
  }

  return <>
      <label>
        FEN String:
        <input
          value={gameStatus.inputFieldValue}
          onChange={handleFenStringChange}
        />
        <div className="validation-container">
           <button className="submitFenButton" onClick={() => validateFenString()} disabled={validateButtonDisabled}>Validate FEN String</button>
           {!validateButtonDisabled &&
              <p className="errorMessage">{errorMessage}</p>
           }       
        </div>
      </label>
      <ChessBoard fenStatus={gameStatus.fenStatus} onPlay={onPlay} selectedRow={gameStatus.selectedRow} selectedColumn={gameStatus.selectedColumn}/>
   </>;
}