import { mapFenStringTo2DMatrix } from './Utils.js';


export function Square( { value, backgroundColor, onSquareClick }) {

  const visibleCheckPiece = mapToVisiblePiece(value); 
  let backgroundClasses = ['square', 'square_' + backgroundColor];

  function mapToVisiblePiece() {
    const fenCheckPieceList = "rnbqkpRNBQKP";
    const asciiCheckPieceList = "♜♞♝♛♚♟♖♘♗♕♔♙"
    const position = fenCheckPieceList.indexOf(value);
    if (value === "" || position === -1) {
      return " ";
    }

    return asciiCheckPieceList[position];
  }

  return (
     <button 
        className={backgroundClasses.join(' ')}
        onClick={onSquareClick}
        >
       {visibleCheckPiece}
     </button>
  );
}


export function ChessBoard( { fenStatus, onPlay, selectedRow, selectedColumn } ) {

  const squareValueRows = mapFenStringTo2DMatrix(fenStatus);

  function handleClick(rowNr, columnNr)  {
    onPlay(rowNr, columnNr)
  }

  function backgroundColor(rowNr, columnNr) {
    let isEvenColumn = columnNr % 2 === 0;
    let isEvenRow = rowNr % 2 === 0;
    if (!isNaN(selectedColumn)) {
      if (selectedRow === rowNr && selectedColumn == columnNr) {
        return 'marked';
      }
    }
    if (isEvenRow) {
      return isEvenColumn ? 'w' : 'b';
    } 
    return isEvenColumn ? 'b' : 'w';
  }

  return (
    <>
    {squareValueRows.map((row,rowId)=>{
      return <div key={rowId} className="board-row">
       {row.map((square,columnId)=>{
         let key = rowId*8 + columnId;
         return <Square key={key} value={square} backgroundColor={backgroundColor(rowId, columnId)} onSquareClick={() => handleClick(rowId, columnId)} />
       })}
      </div>
    })}

    </>
  );
}