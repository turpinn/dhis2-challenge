import { pieceColor, mapFenStringTo2DMatrix, map2DMatrixToFenString } from './Utils.js';

const LOCAL_STORAGE_KEY = "DHIS2_BOARD_STATUS";

export function getInitialFenStatus() {
  const lastStoredValue = localStorage.getItem(LOCAL_STORAGE_KEY);
  console.log(lastStoredValue);
  if (!lastStoredValue) {
    return 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR';
  }
  return lastStoredValue;
}

export function chessReducer(gameStatus, action) {
  console.log(action.type);
  switch (action.type) {
    case 'setFenStatus': {
      localStorage.setItem(LOCAL_STORAGE_KEY, action.newFenStatus);
      return {
        ...gameStatus,
        fenStatus: action.newFenStatus,
        selectedRow: NaN,
        selectedColumn: NaN,
      }
    }
    case 'setInputFieldValue': {
      return {
        ...gameStatus,
        inputFieldValue: action.newValue,
      }
    }
    case 'setSelected': {
      let boardMatrix = mapFenStringTo2DMatrix(gameStatus.fenStatus);
      const valueInTargetSquare = boardMatrix[action.row][action.column];

      // Do not select square if it is empty
      if (valueInTargetSquare === " ") {
        return {
          ...gameStatus
        }
      }

      return {
        ...gameStatus,
        selectedRow: action.row,
        selectedColumn: action.column,
      }
    }
    case 'undoSelection': {
      return {
        ...gameStatus,
        selectedRow: NaN,
        selectedColumn: NaN,
      }
    }
    case 'moveToOrToggle': {
      let boardMatrix = mapFenStringTo2DMatrix(gameStatus.fenStatus);
      const currentlySelectedValue = boardMatrix[gameStatus.selectedRow][gameStatus.selectedColumn];
      const targetValue = boardMatrix[action.row][action.column];
      const targetColor = pieceColor(targetValue);
      const sourceColor = pieceColor(currentlySelectedValue);

      // Toggle selection to another piece of same color
      if (sourceColor === targetColor) {
        return {
          ...gameStatus,
          selectedRow: action.row,
          selectedColumn: action.column,
        }
      }

      // Move to empty place or replace piece of opposite color
      boardMatrix[gameStatus.selectedRow][gameStatus.selectedColumn] = " ";
      boardMatrix[action.row][action.column] = currentlySelectedValue;
      const updatedFedStatus = map2DMatrixToFenString(boardMatrix);
      localStorage.setItem(LOCAL_STORAGE_KEY, updatedFedStatus);
      return {
        inputFieldValue: updatedFedStatus,
        fenStatus: updatedFedStatus,
        selectedRow: NaN,
        selectedColumn: NaN,
      }   
    }
    default: {
      throw Error('Unknown action: ' + action.type);
    }
  }
}