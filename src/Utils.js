function mapFenRowToValuesRow(fenRow) {
    var result = [];
    for(let character of fenRow) {
      let asInt = parseInt(character);
      if (isNaN(asInt)) {
        result.push('' + character);
      } else {
        var emptySquares = new Array(asInt).fill(" ");
        result.push(...emptySquares);
      }
    }
    return result;
  }
  
  export function mapFenStringTo2DMatrix(fenStatus) {
    return fenStatus.split('/').map((x) => mapFenRowToValuesRow(x));
  }
  
  export function map2DMatrixToFenString(chessMatrix) {
    function mapMatrixRowToFenRow(matrixRow) {
      var result = "";
      var emptyCounter = 0;
      for(let value of matrixRow) {
        if (value !== " ") {
          if (emptyCounter > 0) {
            result += String(emptyCounter);
            emptyCounter = 0;
          }
          result += value;
        } else {
          emptyCounter += 1;
        }
      }
      if (emptyCounter > 0) {
        result += String(emptyCounter);
      }
      return result;
    }
  
    return chessMatrix.map(x => mapMatrixRowToFenRow(x)).join('/');
  }
  
  export function isValidFenString(fenString) {
  
    let rows = fenString.split('/')
    if (rows.length !== 8) {
      return false;
    }
  
    let validFenChars = 'rnbqkbnrpRNBQKBNRP12345678';
  
    for (let row of rows) {
      if (row.length === 0) {
        return false;
      }

      for(let character of row) {
        if (validFenChars.indexOf(character) === -1) {
          return false;
        }
  
        if (mapFenRowToValuesRow(row).length !== 8) {
          return false;
        }
      }
    }
  
    return true;
  }
  
  export function pieceColor(pieceFenValue) {
    // Handle empty place
    if (pieceFenValue == " ") {
      return " ";
    }
  
    if (pieceFenValue == pieceFenValue.toUpperCase()) {
      return "w";
    }
  
    return "b";
  }