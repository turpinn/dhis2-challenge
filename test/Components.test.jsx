import {describe, expect, test} from 'vitest';
import {render, screen} from '@testing-library/react';
import {Square} from '../src/Components.jsx';

describe("Square test", () => {
    test("should display black queen", () => {
        render(<Square value='q' backgroundColor='w' onSquareClick='{}' />);
        expect(screen.getByText(/♛/i)).toBeDefined()
    })
})