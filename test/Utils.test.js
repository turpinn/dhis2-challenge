import { expect, test } from 'vitest'
import { pieceColor, isValidFenString } from '../src/Utils.js'

test('FEN string validation, sunshine cases', () => {
  expect(isValidFenString('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR')).toBeTruthy();
  expect(isValidFenString('rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR')).toBeTruthy();
  expect(isValidFenString('rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR')).toBeTruthy();
  expect(isValidFenString('rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R')).toBeTruthy();
  expect(isValidFenString('8/8/8/8/8/8/8/8')).toBeTruthy();
  expect(isValidFenString('8/8/8/8/8/8/8/q7')).toBeTruthy();
})

test('FEN string validation, error cases', () => {
    expect(isValidFenString('rnbqkbnr/pppXpppp/8/8/8/8/PPPPPPPP/RNBQKBNR')).toBeFalsy();
    expect(isValidFenString('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBN')).toBeFalsy();
    expect(isValidFenString('')).toBeFalsy();
    expect(isValidFenString('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/')).toBeFalsy();
    expect(isValidFenString('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/P')).toBeFalsy();
    expect(isValidFenString('8/8/8/8/8/8/8/7')).toBeFalsy();
  })

test('Detects piece color, or empty space', () => {
    expect(pieceColor('P')).toBe('w');
    expect(pieceColor('p')).toBe('b');
    expect(pieceColor(' ')).toBe(' ');
  })